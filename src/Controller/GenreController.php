<?php

namespace App\Controller;

use App\Entity\Genre;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormFactoryInterface;
use App\Form\GenreType;
use Symfony\Component\HttpFoundation\Request;

class GenreController extends AbstractController
{
    /**
     * @Route("/genre", name="genre")
     */
    public function index(Request $request, ManagerRegistry $doctrine, FormFactoryInterface $formFactory)
    {
        $genres = $doctrine->getRepository(Genre::class)->findAll();
        $nbrGenres = $doctrine->getRepository(Genre::class)->countAll();
        $form = $formFactory->createBuilder(GenreType::class)->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // TO DO

        }

        return $this->render('genre/index.html.twig', [
            'genres' => $genres,
            'form' => $form->createView(),
            'total' => $nbrGenres[0]['total']
        ]);
    }
}