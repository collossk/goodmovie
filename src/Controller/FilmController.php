<?php

namespace App\Controller;

use App\Entity\Film;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class FilmController extends AbstractController
{
    /**
     * @Route("/film", name="film")
     */
    public function index(ManagerRegistry $doctrine)
    {
        $films = $doctrine->getRepository(Film::class)->findAll();

        return $this->render('film/index.html.twig', compact('films'));
    }
}
