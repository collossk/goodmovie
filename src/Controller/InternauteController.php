<?php

namespace App\Controller;

use App\Entity\Internaute;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;

class InternauteController extends AbstractController
{
    /**
     * @Route("/internaute", name="internaute")
     */
    public function index()
    {
        $internautes = $this->getDoctrine()
            ->getRepository(Internaute::class)
            ->findAll();

        return $this->render('internaute/index.html.twig', compact('internautes'));
    }

    /**
     * @Route("/internaute/ajout", name="ajout_internaute")
     */
    public function ajoutInternaute(Request $request)
    {
        $item = new Internaute();

        $item->setEmail('');
        $item->setNom('');
        $item->setPrenom('');
        $item->setRegion('');

        $form = $this->createFormBuilder($item)
            ->add('email', EmailType::class)
            ->add('nom', TextType::class)
            ->add('prenom', TextType::class)
            ->add('region', TextType::class)
            ->getForm();

        // Par défaut, le formulaire renvoie un POST au même contrôleur qui la restitue.
        if ($request->isMethod('POST')) {
            $form->submit($request->request->get($form->getName()));

            if ($form->isSubmitted() && $form->isValid()) {
                if ($form->isSubmitted() && $form->isValid()) {
                    $item = $form->getData();

                    $em = $this->getDoctrine()->getManager();
                    $em->persist($item);
                    $em->flush();

                    return $this->redirectToRoute('internaute');
                }

                return $this->redirectToRoute('internaute');
            }
        }

        return $this->render('internaute/ajout.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/internaute/supprimer/{id}", name="supprimer_internaute")
     */
    public function supprimerInternaute($id)
    {
        $item = $this->getDoctrine()
            ->getRepository(Internaute::class)
            ->find($id);

        if (!$item) {
            throw $this->createNotFoundException(
                'No product found for id ' . $id
            );
           } else {
            $em = $this->getDoctrine()->getManager();
            $em->remove($item);
            $em->flush();
            }

            return $this->redirectToRoute('internaute');
        }

    /**
     * @Route("/internaute/modifier/{id}", name="modifier_internaute")
     */
    public function modifierInternaute(Request $request, $id)
    {
        $item = $this->getDoctrine()
            ->getRepository(Internaute::class)
            ->find($id);

        if (!$item) {
            throw $this->createNotFoundException(
                'No product found for id ' . $id
            );
        } else {
            $form = $this->createFormBuilder($item)
                ->add('email', EmailType::class)
                ->add('nom', TextType::class)
                ->add('prenom', TextType::class)
                ->add('region', TextType::class)
                ->getForm();
        }

        // Par défaut, demande POST au même contrôleur qui la restitue.
        if ($request->isMethod('POST')) {
            $form->submit($request->request->get($form->getName()));

            if ($form->isSubmitted() && $form->isValid()) {
                $item = $form->getData();
                $em = $this->getDoctrine()->getManager();
                $em->persist($item);
                $em->flush();

                return $this->redirectToRoute('internaute');
            }
        }

        return $this->render('internaute/ajout.html.twig', array(
            'form' => $form->createView(),
        ));
    }
}